<?php

namespace App\Http\Controllers;

use App\Photo;
use App\Application;

class AdminController extends Controller
{
    public function index()
    {
        $applicationCount = Application::where('id', '>', 0)->count();

        // photocount is only for paid up applications
        $applications = Application::with('photos')->whereNotNull('txn_id')->get();

        $photoCount = 0;
        foreach($applications as $application){
            $photoCount += $application->photos->count();
        }
       

        return view('admin.index', compact('applicationCount', 'photoCount'));
    }

}
