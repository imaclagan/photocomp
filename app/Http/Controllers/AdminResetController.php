<?php

namespace App\Http\Controllers;

use App\Utility\Utils;
use Illuminate\Support\Facades\DB;

class AdminResetController extends Controller
{
    public function reset()
    {

        
        
        // Check this is the admin user
        if(! auth()->user()->id == 1){
            flash('Bad request');
            back();
        }
        
        // Check the compitition is closed - it needs to be
        if (strtolower($this->setting('competition_status')) !== 'closed') {
            flash('Cannot reset the application unless the competition status is "closed"');
            back();
        }

        

        // remove all photos and Images
        DB::delete('delete from photos');//ensures all photos will be orphans
        Utils::trashOrphanPhotos(); 

        // Remove all images from exportphotos folder
        \Storage::disk('local')->deleteDirectory('exportphotos');
        \Storage::disk('local')->makeDirectory('exportphotos');
        // $exportFiles = \Storage::disk('exportphotos')->allFiles('');
        // dd($exportFiles);

        
        

        // remove all Users - except admin
        // Commented out next line as they like to keep the list of users from last year
        DB::delete('delete from users where `is_admin` = "no"');

        // remove all Applications
        DB::delete('delete from applications');

        flash('Application reset completed');
        return back();
    }
}
