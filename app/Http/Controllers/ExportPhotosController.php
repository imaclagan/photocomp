<?php

namespace App\Http\Controllers;

use App\Photo;
use Carbon\Carbon;
use App\Application;
use App\Jobs\ExportPhoto;
use App\Jobs\SetupForPhotoExport;
use Illuminate\Support\Facades\Storage;

class ExportPhotosController extends Controller
{
   
   public function export_dashboard()
   {
    {
        // Delete all old export files in export folder
        $exportFiles = Storage::disk('exportphotos')->files();
        foreach($exportFiles as $file){
            Storage::disk('exportphotos')->delete($file);
        }

        Storage::disk('local')->put('logs/export.log', 'Photo  export date: ' . Carbon::now()->toFormattedDateString());
        
        
        
        $applications = Application::whereNotNull('txn_id')->select('id')->get();

        $applicationIds = array_values($applications->pluck('id')->toArray());
        
        return view('admin.export_dashboard',['applicationIds'=>$applicationIds]);
    }
   }
   
    public function export($id = null)
    {


        
        $application = Application::find($id);
       


        $photos = Photo::where('user_id', $application->user_id)
            ->orderBy('section_id', 'asc')
            ->orderBy('section_entry_number', 'asc')
            ->get(); // Always export ALL photos

    
       

        //$files = Storage::disk('photos')->files();

        foreach($photos as $photo){

            if(! Storage::disk('photos')->exists($photo->filepath)) {
                // Delete the photo record as file does not exits!!!
                
                Storage::append('logs/export.log',  'Deleted Orphan Phote DB record - ' . $photo->export_filename);
                // $photo->delete();
                continue;
            }

            $fileContents = Storage::disk('photos')->get( $photo->filepath);
            Storage::disk('exportphotos')->put($photo->export_filename,$fileContents);
            Storage::append('logs/export.log', $photo->updated_at . ' - ' . $photo->export_filename);

        }

        

        
        
        // Copy photos to export folder
        // foreach($files as $filepath) {

        //     $photo = Photo::where('filepath',$filepath)->first();

        //     // TODO check if file exists and skip if not. Happens occasionally when a file has been deleted but is still in db??
        //     if(! Storage::disk('photos')->exists($filepath)) {
        //         // Delete the photo record as file does not exits!!!
                
        //         Storage::append('logs/export.log',  'Deleted Orphan Phote DB record - ' . $photo->export_filename);
        //         // $photo->delete();
        //         continue;
        //     }
        //     $fileContents = Storage::disk('photos')->get( $filepath);

            
        //     if($photo){
        //         Storage::disk('exportphotos')->put($photo->export_filename,$fileContents);
        //         Storage::append('logs/export.log', $photo->updated_at . ' - ' . $photo->export_filename);
        //     }
            
        // }
        
        // dd('export done');

        

        // Storage::disk('s3')->put(env('AWS_EXPORT_FOLDER') . '/' . $this->photo->export_filename, $fileContents);

        // Compile the list of photo export jobs
        // $photoExports = $photos->map(function ($photo) {
        //     return new \App\Jobs\ExportPhoto($photo);
        // });


        // Dispatch the job
        // SetupForPhotoExport::withChain($photoExports->toArray())->dispatch()->allOnConnection('database')->allOnQueue('export');

        // Display a view that displays the number  of
        // photos NOT exported. ie via ajax query
        // return view('admin.export_monitor');
        return $this->Jsend('success',$id);

    }
}
