<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Application;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        return view('admin.settings_form');
    }

    public function update(Request $request)
    {
        $settings = Setting::first();

        

        $validatedData = $request->validate([
            'title' => 'required',
            'first_section_cost' => 'nullable',
            'digital_only_entry_surcharge' => 'required|numeric|min:0',
            'max_entries_per_section' => 'required|integer|min:1|max:10',
            'terms_and_conditions_url' => 'required',
            'competition_status' => 'required',
            'return_instructions' => 'required',
            'paypal_mode' => 'required',
            'flagfall_cost' => 'required|numeric|min:0',
            'digital_section_cost' => 'required|numeric|min:0',
            'print_section_cost' => 'required|numeric|min:0',
            'result_report_top_text_block'=>'required|min:50|max:254',
            'result_report_main_text_block'=> 'required|min:250|max:1540'
    
        ]);

         //dd($validatedData['competition_status']);

        //$data = $request->except(['save_btn', '_token']);

        $settings->update($validatedData);



        // TODO Check if competition has been closed and delete sensitive user data from applications table
        if($validatedData['competition_status'] == 'Closed'){

           // dd('TODO clear out sensitive Application data');
            // $clearDataFields = [
            //     'lastname'=> '',
            //     'phone' => '',
            // ];
            
            // Application::update($clearDataFields);

        }
        
        

        return back();
    }

}
