<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GeneralMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $content;
    public $firstname;
    public $lastname;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->subject = $email['subject'];
        $this->content = $email['content'];
        $this->firstname = $email['firstname'];
        $this->lastname = $email['lastname'];

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
            ->view('emails.message');
    }
}
