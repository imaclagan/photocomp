@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">


			<h1 style="margin-bottom:100px">Photo Export Monitor</h1>
			<div id="prompter" style="max-width:400px"></div>

			

		</div>
	</div>
</div>



@endsection

@section('scripts')
<script>

var $apiToken = "{{ Auth::user()->api_token }}";
var $postUrl = "{{ url('/admin/export/photos') }}"

window.onload = function () {
	var applications = <?php echo json_encode($applicationIds); ?>;

	var prompter = document.getElementById('prompter');

	var counter = applications.length;

	prompter.innerHTML = 'Export progress ...';
	export_photos();
		

	function export_photos(){
		
			application = applications.shift();

			if(typeof application !== "undefined"){

			
				// Make xhr call to export applicants photos
				$.ajax({
					method: "POST",
					dataType: "json",
					url: $postUrl + "/" + application,
					data: {
					api_token: $apiToken
					}
				})
				.done(function (response) {
					//console.log(response);
					prompter.innerHTML += " .." + counter--;
					export_photos();


				})
			} else {
				//console.log('all done');
				prompter.innerHTML += '... export complete';
			}
			
	}
	
	

    

}

</script>
@endsection
