@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

			<h1>Admin Dashboard</h1>
			<hr>
			<h3>Setup</h3>
			<p><a href="{{ route('admin.settings') }}">Edit Competition Settings</a></p>
			<p><a href="{{ route('admin.clubs') }}">Edit list of VAPS Clubs</a></p>
			<p><a href="{{ route('admin.category.form') }}">Edit Competition Categories</a></p>
			<p><a href="{{ route('admin.credentials.form') }}">Change the Administrator</a></p>
			<hr>
			<h3>Accounts</h3>
			<p><a href="{{ route('admin.accounts.list') }}">List Accounts</a></p>
			<hr>

			<h3>Applications ({{ $applicationCount }})</h3>
			<p><a href="{{ route('admin.applications') }}">List Entrant Applications</a></p>
			<p><a href="{{ route('admin.messaging') }}">Message (email) Applicants</a></p>
			<hr>
			<h3>Export Photos ({{ $photoCount }})</h3>
			<p>The export photos option will export photos to the <b>exportphotos</b> folder on the server with a customised filename in the form of:<br />


			{section_number}_{section_entry_number}_{application_number}_{entrant_firstname entrant_lastname}.jpg</p>
			<p>The exporting process may take a while depending on the number of photos to be exported</p>
			<p>Use an FTP client to download the exported photos. </p>
			<p style="padding:15px; background:#f55; color: white">NOTE: The export process first deletes all photos currently stored in the exportphotos folder.

			<p><a href="{{ route('admin.export.photos') }}">Export Photos to the exportphotos folder</a></p>

			@if($settings->competition_status == 'Closed')
			<hr>
			<h3>Master Reset</h3>
			<div style="padding:15px; background:#f55; color: white">
				<p>BE WARNED - Doing a MASTER RESET will :</p>
				<ul>
					<li>LEAVE all user login accounts</li>
					<li>DELETE all photos and application details</li>
				</ul>
			</div>
			<p>The MASTER RESET is intended to be run well after the competition has finished and no one will need access to any competition data or photos anymore.</p>
			<p><a href="{{ route('admin.master.reset') }}" onclick="return confirm('Are you sure you want to perform a Master Reset?')">Master Reset Button!!!</a></p>
			@endif

		</div>
	</div>
</div>

@endsection
